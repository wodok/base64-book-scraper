import re
import base64

# Open the file and read its contents
f = open('base64.txt', 'r')
content = f.read().encode("ascii", "ignore").decode()

# Search for the base64 encoded string in the content
matchObj = re.findall(r'curl \'data\:image\/jpeg\;base64\,([^\']+)\'', content)

# If the match is found, decode and write the image to a file
for i, item in enumerate(matchObj):
   with open(f"image_{i:04}.jpg", "wb") as fh:
    fh.write(base64.decodebytes(item.encode()))
    print(f"Generated {i+1}/{len(matchObj)}")