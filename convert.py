import os
from PIL import Image
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
import glob

# Path to the output PDF file
pdf_path = "out.pdf"

# List all the image file paths in the current directory that match the pattern "image_*.jpg"
img_paths = glob.glob("image_*.jpg")


# Create a new PDF canvas
pdf_canvas = canvas.Canvas(pdf_path, pagesize=letter)

# Loop through all the image paths
for img_path in img_paths:
    with Image.open(img_path) as img:
        width, height = img.size
        pdf_canvas.setPageSize((width, height))
        pdf_canvas.drawImage(img_path, 0, 0, width, height)
        pdf_canvas.showPage()


# Save the PDF canvas to the output file
pdf_canvas.save()

print(f"PDF file '{pdf_path}' created successfully.")
