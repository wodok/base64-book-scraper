from PIL import Image
import os

# Set the output PDF file name and quality
pdf_name = "out.pdf"
pdf_quality = 95

# Get a sorted list of image files in the current directory
image_files = sorted([f for f in os.listdir('.') if f.startswith("image_") and f.endswith(".jpg")])

# Open the first image file to get its size
with Image.open(image_files[0]) as img:
    pdf_size = img.size

# Create a new PDF file and add the images to it
with Image.new('RGB', pdf_size, (255, 255, 255)) as pdf:
    for image_file in image_files:
        with Image.open(image_file) as img:
            pdf.paste(img)
    pdf.save(pdf_name, "PDF", quality=pdf_quality)

print(f"PDF file '{pdf_name}' created successfully.")
