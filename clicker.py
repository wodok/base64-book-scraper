from random import randint
import pyautogui
import time
import sys

def simulate_user_input():
    """Simulate user input by randomly pressing keys or clicking the mouse."""
    time.sleep(10)
    while True:
        state = (randint(0, 2) == 1)
        if state == 0:
            pyautogui.hotkey('alt', 'x')
        elif state == 1:
            pyautogui.hotkey('space')
        else:
            pyautogui.click()
        time.sleep(randint(2, 5))

if __name__ == '__main__':
    try:
        simulate_user_input()
    except Exception as e:
        print(f"Error occurred: {e}")
        sys.exit(1)